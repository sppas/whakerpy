"""

This package name is `whakerpy` which stands for "Web HTML maker in Python".

"""

from .htmlmaker import *
from .httpd import *
from .messages import error
